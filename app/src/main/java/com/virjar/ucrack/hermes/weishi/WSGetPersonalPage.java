package com.virjar.ucrack.hermes.weishi;

import com.virjar.hermes.hermesagent.hermes_api.ActionRequestHandler;
import com.virjar.hermes.hermesagent.hermes_api.WrapperAction;
import com.virjar.hermes.hermesagent.hermes_api.aidl.InvokeRequest;
import com.virjar.hermes.hermesagent.hermes_api.aidl.InvokeResult;
import com.virjar.xposed_extention.ClassLoadMonitor;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import de.robv.android.xposed.XposedHelpers;

@WrapperAction("WSGetPersonalPage")
public class WSGetPersonalPage implements ActionRequestHandler {
    public static WSGetPersonalPage instance = null;

    public WSGetPersonalPage() {
        instance = this;
    }

    @Override
    public Object handleRequest(InvokeRequest invokeRequest) {
        String userID = invokeRequest.getString("userID");
        if (StringUtils.isBlank(userID)) {
            return InvokeResult.failed("the param {userID} not presented");
        }
        int type = NumberUtils.toInt(invokeRequest.getString("type"), 0);
        String attachInfo = invokeRequest.getString("attach_info");
        int entrance = NumberUtils.toInt(invokeRequest.getString("entrance"));
        Class modelClass = ClassLoadMonitor.tryLoadClass("com.tencent.oscar.module.e.b.a.b");
        if (modelClass != null) {
            return XposedHelpers.newInstance(modelClass
                    , userID, type, attachInfo, entrance);
        }
        return WeishiUtil.sendRequest(XposedHelpers.newInstance(ClassLoadMonitor.tryLoadClass("com.tencent.oscar.module.f.b.a.b")
                , userID, type, attachInfo, entrance));
    }
}
